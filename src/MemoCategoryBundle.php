<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\CategoryBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MemoCategoryBundle extends Bundle
{
}
