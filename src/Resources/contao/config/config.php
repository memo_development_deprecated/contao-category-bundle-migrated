<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Add tl_memo_category to backend menu
 */

use Contao\System;
use Symfony\Component\HttpFoundation\Request;

$GLOBALS['BE_MOD']['content']['memo_category']['tables'][] = 'tl_memo_category';

/**
 * Frontend Modules
 */
$GLOBALS['FE_MOD']['memo_category']['category_filter'] = 'Memo\CategoryBundle\Module\ModuleFilter';

/**
 * Models
 */
$GLOBALS['TL_MODELS']['tl_memo_category'] = 'Memo\CategoryBundle\Model\CategoryModel';

/**
 * Backend CSS
 */
if (System::getContainer()->get('contao.routing.scope_matcher')->isBackendRequest(System::getContainer()->get('request_stack')->getCurrentRequest() ?? Request::create('')))
{
    // Define CSS File
    $strCSSFileURL = 'bundles/memocategory/backend.css';
    $strCSSFilePath = 'vendor/memo_development/contao-category-bundle/src/Resources/public/backend.css';

    // Get File mtimestamp
    $strRootDir = System::getContainer()->getParameter('kernel.project_dir');
    $strCSSFileTimestamp = filemtime($strRootDir . '/' . $strCSSFilePath);
    $GLOBALS['TL_CSS'][]        = $strCSSFileURL . '?v=' . $strCSSFileTimestamp;
}
