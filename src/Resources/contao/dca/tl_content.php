<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\Controller;
use Contao\System;

Controller::loadDataContainer('tl_module');
System::loadLanguageFile('tl_module');

$GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['categories_filter'];
$GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['eval']['doNotSaveEmpty'] = false;
$GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['relation']['referenceColumn'] = 'content_id';
$GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['relation']['relationTable'] = 'tl_memo_content_categories_filter';

// Remove the relation from the field (not needed for the content element)
if(isset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['relation'])){
    unset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['relation']);
}

// Remove the load_callback from haste dca relations (not needed for the content element)
if(isset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['load_callback']) && is_array($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['load_callback'])){
    foreach($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['load_callback'] as $intKey => $arrCallback){
        if($arrCallback[0] == 'Codefog\HasteBundle\DcaRelationsManager'){
            unset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['load_callback'][$intKey]);
        }
    }
}

// Remove the load_callback from haste dca relations (not needed for the content element)
if(isset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['save_callback']) && is_array($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['save_callback'])){
    foreach($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['save_callback'] as $intKey => $arrCallback){
        if($arrCallback[0] == 'Codefog\HasteBundle\DcaRelationsManager'){
            unset($GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter']['save_callback'][$intKey]);
        }
    }
}

$GLOBALS['TL_DCA']['tl_content']['fields']['categories_filter_type'] = &$GLOBALS['TL_DCA']['tl_module']['fields']['categories_filter_type'];
