<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Memo\CategoryBundle\Service\CategoryService;

$GLOBALS['TL_DCA']['tl_module']['palettes']['category_filter'] = '{title_legend}, name, type;{config_legend}, categories,category_levels;{template_legend}, customTpl;{expert_legend:hide},cssID';


/**
 * Add palettes to tl_module
 */
$GLOBALS['TL_DCA']['tl_module']['fields']['categories'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['categories'],
    'exclude' => true,
    'filter' => false,
    'inputType' => 'select',
    'foreignKey' => 'tl_memo_category.title',
    'options_callback' => ['memo.foundation.category', 'getCategoryGroups'],
    'eval' => array(
        'multiple' => true,
        'chosen' => true,
        'tl_class' => 'clr long'
    ),
    'relation' => array(
        'type' => 'haste-ManyToMany',
        'load' => 'lazy',
        'table' => 'tl_memo_category',
        'referenceColumn' => 'module_id',
        'fieldColumn' => 'category_id',
        'relationTable' => 'tl_memo_module_categories',
        'forceSave' => true,
    ),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['categories_filter'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['categories_filter'],
    'exclude' => true,
    'filter' => false,
    'inputType' => 'checkbox',
    'foreignKey' => 'tl_memo_category.title',
    'options_callback' => ['memo.foundation.category', 'getAllCategories'],
    'eval' => array(
        'multiple' => true,
        'doNotSaveEmpty' => false,
        'tl_class' => 'clr long'
    ),
    'relation' => array(
        'type' => 'haste-ManyToMany',
        'load' => 'lazy',
        'table' => 'tl_memo_category',
        'referenceColumn' => 'module_id',
        'fieldColumn' => 'category_id',
        'relationTable' => 'tl_memo_module_categories_filter',
        'forceSave' => true,
    ),
    'sql' => "blob NULL"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['categories_filter_type'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['categories_filter_type'],
    'exclude' => true,
    'filter' => false,
    'inputType' => 'select',
    'options' => array('and' => 'Eintrag muss ALLE Kategorien haben', 'or' => 'Eintrag muss EINE der Kategorien haben'),
    'eval' => array(
        'multiple' => false,
        'chosen' => true,
        'tl_class' => 'clr w50'
    ),
    'sql' => "varchar(15) NOT NULL default 'and'"
);

$GLOBALS['TL_DCA']['tl_module']['fields']['category_levels'] = array
(
    'label' => &$GLOBALS['TL_LANG']['tl_module']['category_levels'],
    'exclude' => true,
    'filter' => false,
    'inputType' => 'checkbox',
    'eval' => array(
        'tl_class' => 'm12 long clr'
    ),
    'sql' => "char(1) NOT NULL default ''",
);

