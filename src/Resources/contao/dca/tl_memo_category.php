<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

use Contao\Backend;
use Contao\Config;
use Contao\DataContainer;
use Contao\DC_Table;
use Contao\System;
use Memo\CategoryBundle\Model\CategoryModel;
use Memo\FoundationBundle\Service\ToolboxService;
use Memo\MemoFoundationBundle\Classes\FoundationBackend;

/**
 * Table tl_memo_category
 */
$GLOBALS['TL_DCA']['tl_memo_category'] = array
(

    // Config
    'config' => array
    (
        'label' => 'Kategorien',
        'dataContainer' => DC_Table::class,
        'switchToEdit' => true,
        'enableVersioning' => true,
        'markAsCopy' => 'title',
        'sql' => array
        (
            'keys' => array
            (
                'id' => 'primary',
                'pid' => 'index',
                'sorting' => 'index'
            )
        )
    ),

    // List
    'list' => array
    (
        'sorting' => array
        (
            'mode' => DataContainer::MODE_TREE,
            'rootPaste' => true,
            'showRootTrails' => true,
            'icon' => 'pagemounts.svg',
            'fields' => array('sorting'),
            'panelLayout' => 'filter;sort,search,limit',
            'headerFields' => array('title', 'alias'),
            'defaultSearchField' => 'title'
        ),
        'label' => array
        (
            'fields' => array('title'),
            'format' => '%s',
            'label_callback' => array('tl_memo_category', 'addIcons')
        ),
        'global_operations' => array
        (
            'toggleNodes' => [
                'label' => &$GLOBALS['TL_LANG']['MSC']['toggleAll'],
                'href' => 'ptg=all',
                'class' => 'header_toggle'
            ],
            'all' => array
            (
                'label' => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href' => 'act=select',
                'class' => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset();" accesskey="e"'
            )
        ),
        'operations' => array
        (
            'edit' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['edit'],
                'href' => 'act=edit',
                'icon' => 'edit.gif'
            ),
            'copy' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['copy'],
                'href' => 'act=copy',
                'icon' => 'copy.gif'
            ),
            'copyChilds' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['copyChilds'],
                'href' => 'act=paste&amp;mode=copy&amp;childs=1',
                'icon' => 'copychilds.gif',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ),
            'cut' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['cut'],
                'href' => 'act=paste&amp;mode=cut',
                'icon' => 'cut.gif',
                'attributes' => 'onclick="Backend.getScrollOffset()"'
            ),
            'delete' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['delete'],
                'href' => 'act=delete',
                'icon' => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . ($GLOBALS['TL_LANG']['MSC']['deleteConfirm'] ?? null) . '\'))return false;Backend.getScrollOffset()"'
            ),
            'toggle' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['toggle'],
                'icon' => 'visible.gif',
                'attributes' => 'onclick="Backend.getScrollOffset();return AjaxRequest.toggleVisibility(this,%s)"',
                'button_callback' => array(
                    'tl_memo_category', 'toggleIcon'
                )
            ),
            'show' => array
            (
                'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['show'],
                'href' => 'act=show',
                'icon' => 'show.gif'
            ),
        )
    ),

    // Select
    'select' => array
    (
        'buttons_callback' => array()
    ),

    // Edit
    'edit' => array
    (
        'buttons_callback' => array()
    ),

    // Palettes
    'palettes' => array
    (
        'default' => '{general_legend}, title, alias, type, color, description; {media_legend},singleSRC; {publish_legend}, published, start, stop;',
    ),

    // Subpalettes
    'subpalettes' => array
    (
        '' => '',
    ),

    // Fields
    'fields' => array
    (
        'id' => array
        (
            'sql' => "int(10) unsigned NOT NULL auto_increment"
        ),
        'pid' => array(
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'sorting' => array
        (
            'label' => &$GLOBALS['TL_LANG']['MSC']['sorting'],
            'sorting' => true,
            'flag' => 11,
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'tstamp' => array
        (
            'sql' => "int(10) unsigned NOT NULL default '0'"
        ),
        'color' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['color'],
            'exclude' => true,
            'inputType' => 'text',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50',
                'colorpicker' => true,
            ),
            'save_callback' => array
            (
                array('tl_memo_category', 'generateColor')
            ),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'title' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['title'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'sql' => "varchar(255) NOT NULL default ''",
        ),
        'alias' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['alias'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => true,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array(
                'rgxp' => 'alias',
                'doNotCopy' => true,
                'maxlength' => 255,
                'tl_class' => 'w50'
            ),
            'save_callback' => array
            (
                array('tl_memo_category', 'generateAlias')
            ),
            'sql' => "varchar(255) BINARY NOT NULL default ''"
        ),
        'type' => array
        (
            'exclude' => true,
            'filter' => true,
            'inputType' => 'select',
            'eval' => array('tl_class' => 'w50'),
            'options' => array('root', 'category'),
            'reference' => &$GLOBALS['TL_LANG']['tl_memo_category']['types'],
            'sql' => "varchar(64) NOT NULL default 'category'"
        ),
        'description' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['description'],
            'translate' => true,
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'textarea',
            'eval' => array(
                'mandatory' => false,
                'rte' => 'tinyMCE',
                'tl_class' => 'clr',
            ),
            'sql' => "mediumtext NULL"
        ),
        'singleSRC' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['singleSRC'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'fileTree',
            'eval' => array(
                'mandatory' => false,
                'filesOnly' => true,
                'extensions' => Config::get('validImageTypes'),
                'fieldType' => 'radio'),
            'sql' => "binary(16) NULL"
        ),
        'hide' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['hide'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'protected' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['protected'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'maintenanceMode' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['maintenanceMode'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'requireItem' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['requireItem'],
            'exclude' => true,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'published' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['published'],
            'exclude' => true,
            'filter' => true,
            'search' => false,
            'sorting' => false,
            'inputType' => 'checkbox',
            'eval' => array(
                'doNotCopy' => true,
                'tl_class' => 'w50'
            ),
            'sql' => "char(1) NOT NULL default ''",
        ),
        'start' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['start'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard clr'),
            'sql' => "varchar(10) NOT NULL default ''"
        ),
        'stop' => array
        (
            'label' => &$GLOBALS['TL_LANG']['tl_memo_category']['stop'],
            'exclude' => true,
            'filter' => false,
            'search' => false,
            'sorting' => false,
            'inputType' => 'text',
            'eval' => array('rgxp' => 'datim', 'datepicker' => true, 'tl_class' => 'w50 wizard'),
            'sql' => "varchar(10) NOT NULL default ''"
        )
    )
);

// Add multi-lang fields automatically by checking for the translate field
$objLanguageService = System::getContainer()->get('memo.foundation.language');
$objLanguageService->generateTranslateDCA('tl_memo_category');


/**
 * Class tl_memo_team
 */
class tl_memo_category extends FoundationBackend
{
    public function listLayout($row)
    {
        return '<div class="tl_content_left">' . '<strong>' . $row['title'] . '</strong></div>';


    }

    public function generateAlias($varValue, DataContainer $dc)
    {
        return $this->generateAliasFoundation($dc, $varValue, 'tl_memo_category', array('title'));
    }

    public function generateColor($varValue, DataContainer $dc)
    {
        if ($varValue == '') {
            $varValue = ToolboxService::stringToHexColor($dc->activeRecord->title, 100, 2);
        }
        return $varValue;
    }

    public function toggleIcon($row, $href, $label, $title, $icon, $attributes)
    {
        return $this->toggleIconFoundation($row, 'published', 'invisible', $icon, $title, $attributes, $label);
    }

    public function toggleVisibility($intId, $blnVisible, DataContainer $dc = null)
    {
        $this->toggleVisibilityFoundation($intId, $blnVisible, $dc, 'tl_memo_category', 'published');
    }

    public function addIcons($row, $label, DataContainer $dc = null, $imageAttribute = '', $blnReturnImage = false, $blnProtected = false)
    {
        $strTitle = Backend::addPageIcon($row, $label, $dc, $imageAttribute, $blnReturnImage, $blnProtected);

        if ($row['type'] == 'category') {
            if ($row['color'] == '') {

                $objCategory = CategoryModel::findByPk($row['id']);
                $row['color'] = ToolboxService::stringToHexColor($objCategory->title, 100, 2);
                $objCategory->color = $row['color'];
                $objCategory->save();
            }

            $strTextColor = ToolboxService::getContrastColor($row['color']);
            $strTitle .= ' <span style="display:inline-block;margin-left:5px; font-size: 0.8em; margin-top:1px;background-color:#' . $row['color'] . ';color:' . $strTextColor . '; padding:2px 7px;border-radius: 7px;">#' . $row['color'] . '</span>';
        }
        return $strTitle;
    }
}
