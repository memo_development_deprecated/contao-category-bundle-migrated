<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
$GLOBALS['TL_LANG']['MOD']['memo_category'] = array("Kategorien", "Kategorie - Frontend Module die von der Media Motion AG entwickelt wurden.");

$GLOBALS['TL_LANG']['FMD']['memo_category'] = array('Kategorien');
$GLOBALS['TL_LANG']['FMD']['category_filter'] = array('Filter');
$GLOBALS['TL_LANG']['FMD']['mod_category_module'] = array('Kategorien');
