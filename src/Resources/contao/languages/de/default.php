<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Miscellaneous
 */

$GLOBALS['TL_LANG']['MSC']['FE']['all'] = 'Alle';
