<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */
/**
 * Legends
 */
$GLOBALS['TL_LANG']['tl_memo_category']['general_legend'] = 'Generelle Informationen';
$GLOBALS['TL_LANG']['tl_memo_category']['media_legend'] = 'Medien';
$GLOBALS['TL_LANG']['tl_memo_category']['publish_legend'] = 'Veröffentlichung';

/**
 * Fields
 */
$GLOBALS['TL_LANG']['tl_memo_category']['color'] = array('Farbe', 'Wird standardmässig nur fürs Backend verwendet.');
$GLOBALS['TL_LANG']['tl_memo_category']['title'] = array('Kategoriename', 'Kategoriename');
$GLOBALS['TL_LANG']['tl_memo_category']['alias'] = array('Alias', 'Wie soll der Alias der Kategorie lauten? Der Alias muss eindeutig sein und wird für die URL verwendet.');
$GLOBALS['TL_LANG']['tl_memo_category']['type'] = array('Typ', 'Handelt es sich bei dieser Kategorie um eine Kategorie-Gruppe oder eine einzelne Kategorie?');
$GLOBALS['TL_LANG']['tl_memo_category']['description'] = array('Beschreibung', 'Eine Beschreibung für eine eventuelle Detailseite');
$GLOBALS['TL_LANG']['tl_memo_category']['singleSRC'] = array('Bild', 'Bild für optionale Bild-Filter, Detailseite etc.');
$GLOBALS['TL_LANG']['tl_memo_category']['published'] = array('Veröffentlicht', 'Soll dieser Kategorie auf der Webseite veröffentlicht werden?');
$GLOBALS['TL_LANG']['tl_memo_category']['start'] = array('Anzeigen ab', 'Wenn Sie die Kategorie erst ab einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');
$GLOBALS['TL_LANG']['tl_memo_category']['stop'] = array('Anzeigen bis', 'Wenn Sie die Kategorie nur bis zu einem bestimmten Zeitpunkt auf der Webseite anzeigen möchten, können Sie diesen hier eingeben. Andernfalls lassen Sie das Feld leer.');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_category']['new'] = array('Neue Kategorie', 'Neue Kategorie anlegen');
$GLOBALS['TL_LANG']['tl_memo_category']['show'] = array('Details', 'Infos zur Kategorie mit der ID %s');
$GLOBALS['TL_LANG']['tl_memo_category']['edit'] = array('Kategorie bearbeiten ', 'Kategorie bearbeiten');
$GLOBALS['TL_LANG']['tl_memo_category']['cut'] = array('Kategorie Verschieben', 'ID %s verschieben');
$GLOBALS['TL_LANG']['tl_memo_category']['copy'] = array('Kategorie Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_category']['copyChilds'] = array('Kategorie und Unterkategorien Duplizieren ', 'ID %s duplizieren');
$GLOBALS['TL_LANG']['tl_memo_category']['delete'] = array('Kategorie Löschen ', 'ID %s löschen');

/**
 * Buttons
 */
$GLOBALS['TL_LANG']['tl_memo_category']['types']['root'] = array('Gruppe', 'Eine Gruppe von Kategorien, z.B. "Teams"');
$GLOBALS['TL_LANG']['tl_memo_category']['types']['category'] = array('Kategorie', 'Einzelne Kategorie, z.B. "Backoffice');
