<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

/**
 * Back end modules
 */
$GLOBALS['TL_LANG']['tl_module']['categories'] = array('Kategorie-Gruppen Filter', 'Filterung der Frontend-Filter nach Kategorie-Gruppen. Dies hat keinen Einfluss auf die Filterauswahl, nur auf die Filter.');
$GLOBALS['TL_LANG']['tl_module']['categories_filter'] = array('Kategorie Filter', 'Filtern Sie nach bestimmten Kategorien, nur Einträge mit diesen Kategorien werden angezeigt.');
$GLOBALS['TL_LANG']['tl_module']['categories_filter_type'] = array('Kategorie Filter Typ', 'Muss ein Eintrag alle (UND) oder nur einzelne Kategorien (ODER) enthalten, um angezeigt zu werden?');
$GLOBALS['TL_LANG']['tl_module']['category_levels'] = array('Kategorien verschachteln?', 'Sollen die Kategorien in einer hirarchischen Struktur (Gruppe > Kategorie) ausgegeben werden?');
