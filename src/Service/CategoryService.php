<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\CategoryBundle\Service;

use Contao\System;
use Memo\CategoryBundle\Model\CategoryModel;
use Memo\FoundationBundle\Service\LanguageService;

class CategoryService
{

    public static function getRootCategories()
    {
        $arrCategories = array();

        if ($objCategories = CategoryModel::findByPid(0)) {
            foreach ($objCategories as $objCategory) {
                $arrCategories[$objCategory->id] = $objCategory->title;
            }
        }

        return $arrCategories;

    }

    public static function getAllCategories($bolLevels = true, $arrCategoryGroups = false, $strLanguage = false)
    {
        // Get the current language
        if (!$strLanguage) {
            $strLanguage = $GLOBALS['TL_LANGUAGE'];
        }

        // Get the default language
        $strDefaultLanguage = System::getContainer()->get('memo.foundation.language')->getDefaultLanguage();

        // Grab all categories
        $arrCategories = array();

        if (is_array($arrCategoryGroups) && count($arrCategoryGroups) > 0) {

            $strCategoryGroups = implode(',', $arrCategoryGroups);

            if ($bolLevels) {
                $colCategories = CategoryModel::findBy(array('pid IN (' . $strCategoryGroups . ') OR id IN (' . $strCategoryGroups . ')'), array(), array('order' => 'pid ASC, sorting'));
            } else {
                $colCategories = CategoryModel::findBy(array('pid IN (' . $strCategoryGroups . ')'), array(), array('order' => 'pid ASC, sorting'));
            }


        } else {

            $colCategories = CategoryModel::findAll(array('order' => 'pid ASC, sorting'));

        }

        // Put them in an array
        if ($colCategories) {

            foreach ($colCategories as $objCategory) {

                // Translate the Attribute
                if ($strLanguage != $strDefaultLanguage) {
                    $objCategory->getTranslatedModel($strLanguage);
                }

                $arrAllCategories[$objCategory->id] = $objCategory;
            }

            if ($bolLevels) {

                // Add Option-Groups
                foreach ($arrAllCategories as $intID => $objCategory) {
                    if ($objCategory->type == 'root') {
                        $arrCategories[$intID] = array();
                    }
                }

                // Add Option-Groups Options
                foreach ($arrAllCategories as $intID => $objCategory) {
                    if (array_key_exists($objCategory->pid, $arrCategories)) {
                        $arrCategories[$objCategory->pid][$intID] = $objCategory->title;
                    }
                }

                // Add Option-Groups Titles (instead of IDs)
                foreach ($arrAllCategories as $intID => $objCategory) {
                    if (array_key_exists($intID, $arrCategories)) {
                        $arrCategories[$objCategory->title] = $arrCategories[$intID];
                        unset($arrCategories[$intID]);
                    }
                }

                // Add direct options (with no parent-element)
                foreach ($arrAllCategories as $intID => $objCategory) {
                    if ($objCategory->type != 'root' && $objCategory->pid == 0) {
                        $arrCategories[$intID] = $objCategory->title;
                    }
                }

            } else {
                $arrCategories = $arrAllCategories;
            }
        }

        return $arrCategories;
    }

    public static function getCategoriesByDC($dc)
    {
        // Get the item-table
        $strItemTable = false;
        if (property_exists($dc, 'table') && $dc->table != '') {
            $strItemTable = $dc->table;
        } elseif (property_exists($dc, 'strTable') && $dc->strTable != '') {
            $strItemTable = $dc->strTable;
        }

        // Get the archive-table and the category-groups
        $arrFilterCategories = false;

        if ($strItemTable) {
            \Controller::loadDataContainer($strItemTable);
            $strArchiveTable = $GLOBALS['TL_DCA'][$strItemTable]['config']['ptable'];
            $strArchiveModel = $GLOBALS['TL_MODELS'][$strArchiveTable];

            // Get request (contao 5+)
            $objRequest = System::getContainer()->get('request_stack')->getCurrentRequest();

            // Get session (contao 5+)
            $objSession = $objRequest->getSession();

            if ($dc->activeRecord->pid) {
                $objSession->set('activeRecordPid', $dc->activeRecord->pid);
                $strActiveRedordPid = $dc->activeRecord->pid;
            } else {
                $strActiveRedordPid = $objSession->get('activeRecordPid');
            }

            $objArchive = $strArchiveModel::findByPk($strActiveRedordPid);

            if (is_string($objArchive->category_groups)) {
                $arrFilterCategories = unserialize($objArchive->category_groups);
            }
        }

        // Grab all categories (for the css classes later on)
        $arrCategories = array();
        $colCategories = CategoryModel::findAll();

        if (!$colCategories) {
            return array();
        }

        // Was only one category defined - in that case, leave out the option-group level
        if (is_array($arrFilterCategories) && count($arrFilterCategories) == 1) {

            foreach ($colCategories as $objCategory) {

                if ($objCategory->pid == $arrFilterCategories[0]) {

                    if ($objCategory->type == 'root') {

                        $arrSubcategorys = array();

                        foreach ($colCategories as $objSubCategory) {
                            if ($objSubCategory->pid == $objCategory->id) {
                                $arrSubcategorys[$objSubCategory->id] = $objSubCategory->title;
                            }
                        }

                        $arrCategories[$objCategory->title] = $arrSubcategorys;

                    } else {
                        $arrCategories[$objCategory->id] = $objCategory->title;
                    }
                }

            }

            return $arrCategories;
        }

        // Put them in an array
        foreach ($colCategories as $objCategory) {
            $arrAllCategories[$objCategory->id] = $objCategory;
        }

        // Add Option-Groups
        foreach ($arrAllCategories as $intID => $objCategory) {
            if ($objCategory->type == 'root' && (!is_array($arrFilterCategories) || is_array($arrFilterCategories) && in_array($intID, $arrFilterCategories))) {
                $arrCategories[$intID] = array();
            }
        }

        // Add Option-Groups Options
        foreach ($arrAllCategories as $intID => $objCategory) {
            if (array_key_exists($objCategory->pid, $arrCategories)) {
                $arrCategories[$objCategory->pid][$intID] = $objCategory->title;
            }
        }

        // Add Option-Groups Titles (instead of IDs)
        foreach ($arrAllCategories as $intID => $objCategory) {
            if (array_key_exists($intID, $arrCategories)) {
                $arrCategories[$objCategory->title] = $arrCategories[$intID];
                unset($arrCategories[$intID]);
            }
        }

        // Add direct options (with no parent-element)
        foreach ($arrAllCategories as $intID => $objCategory) {
            if ($objCategory->type != 'root' && $objCategory->pid == 0 && (!is_array($arrFilterCategories) || is_array($arrFilterCategories) && in_array($intID, $arrFilterCategories))) {
                $arrCategories[$intID] = $objCategory->title;
            }
        }

        return $arrCategories;
    }

    public static function getCategoryGroups()
    {
        // Grab all category-groups
        $arrCategoryGroup = array();
        $colCategoryGroups = CategoryModel::findBy(array('type=?'), array('root'));

        if (!$colCategoryGroups) {
            return array();
        }

        foreach ($colCategoryGroups as $objCategory) {
            $arrAllCategoryGroups[$objCategory->id] = $objCategory->title;
        }

        return $arrAllCategoryGroups;
    }

    public static function resolveCategories($strSerializedVallues, $strLanguage = false)
    {

        // Handle Language
        if (!$strLanguage) {
            $strLanguage = $GLOBALS['TL_LANGUAGE'];
        }
        $strDefaultLanguage = LanguageService::getDefaultLanguage();

        $arrData = unserialize($strSerializedVallues);
        $arrValues = array();

        if (is_array($arrData)) {
            foreach ($arrData as $arrData) {
                $objCategory = CategoryModel::findByPk($arrData['category_id']);
                if ($objCategory) {

                    if ($strLanguage != $strDefaultLanguage) {
                        $objCategory->getTranslatedModel($strLanguage);
                    }

                    $arrValues[] = $objCategory->title;
                }
            }
        }

        return $arrValues;
    }
}
