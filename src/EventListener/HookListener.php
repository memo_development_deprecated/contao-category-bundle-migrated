<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\CategoryBundle\EventListener;

use Contao\CoreBundle\Monolog\ContaoContext;
use Psr\Log\LogLevel;
use Terminal42\ChangeLanguage\Event\ChangelanguageNavigationEvent;
use Contao\CoreBundle\ServiceAnnotation\Hook;
use Memo\FoundationBundle\EventListener\FoundationHookListener;

class HookListener extends FoundationHookListener
{

}
