<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\CategoryBundle\Module;

use Contao\System;
use Symfony\Component\HttpFoundation\Request;
use Memo\FoundationBundle\Module\FoundationModule;
use Memo\CategoryBundle\Service\CategoryService;
use Terminal42\ChangeLanguage\PageFinder;

class ModuleFilter extends FoundationModule
{
    /**
     * Template
     * @var string
     */
    protected $strTemplate = 'mod_category_filter';

    protected function compile()
    {
        $arrCategoryGroups = false;

        // Get all selected Categories
        if (!is_null($this->categories)) {

            $arrCategoryGroups = unserialize($this->categories);
        }

        $arrCategories = CategoryService::getAllCategories($this->category_level, $arrCategoryGroups);

        $this->Template->arrCategories = $arrCategories;

        if (gettype($this->Template->cssID) == 'array' && $this->Template->cssID) {
            $this->Template->cssID = $this->Template->cssID[0];
        } else {
            $this->Template->cssID = '';
        }

        // Get custom Template
        if ($this->customTpl) {
            $this->Template->strTemplate = $this->customTpl;
        }

        // Parse Template
        $this->Template->parse();
    }
}
