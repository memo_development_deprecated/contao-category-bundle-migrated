<?php declare(strict_types=1);

/**
 * @package   Memo\MemoCategoryBundle
 * @author    Media Motion AG
 * @license   LGPL-3.0+
 * @copyright Media Motion AG
 */

namespace Memo\CategoryBundle\Model;

use Memo\FoundationBundle\Model\FoundationModel;

/**
 * Class CategoryModel
 * @package Memo\CategoryBundle\Model
 */
class CategoryModel extends FoundationModel
{
    /**
     * Table name
     * @var string
     */
    protected static $strTable = 'tl_memo_category';
}
