# Memo Category Bundle

## About

With the Category Bundle you can add categories in a pagetree-like structure.
The categories can be used in all compatible foundation-bundles - for visible output or filtering.

## Table of contents

> * [Memo Category Bundle](#title--repository-name)
>   * [About](#about)
>   * [Table of contents](#table-of-contents)
>   * [Installation](#installation)
>   * [Contributing / Reporting issues](#contributing--reporting-issues)
>   * [License](#license)

## Installation

Install [composer](https://getcomposer.org) if you haven't already.
Add the unlisted Repo(s) (not on packagist.org) to your composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-foundation-bundle.git"
  },
  {
    "type": "vcs",
    "url" : "https://bitbucket.org/memo_development/contao-category-bundle.git"
  }
],
```

Add the bundle to your requirements:
```
"memo_development/contao-foundation-bundle": "^2.0",
"memo_development/contao-category-bundle": "^2.0",
```

## Contributing / Reporting issues

Bug reports and pull requests are welcome - via Bitbucket-Issues:
[Bitbucket Issues](https://bitbucket.org/memo_development/contao-category-bundle/issues?status=new&status=open)

## License

[GNU Lesser General Public License, Version 3.0](https://www.gnu.de/documents/lgpl-3.0.de.html)
